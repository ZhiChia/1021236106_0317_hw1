//
//  ViewController.swift
//  Test
//
//  Created by apple on 2017/3/17.
//  Copyright © 2017年 apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func show1(){
        let alert = UIAlertController(title:"小惡魔",message:"我是小惡魔", preferredStyle:UIAlertControllerStyle.alert);
        alert.addAction(UIAlertAction(title:"OK",style:UIAlertActionStyle.default,handler:nil));
        
        present(alert,animated: true,completion: nil);
        
        
    }
    
    @IBAction func show2(){
        let alert = UIAlertController(title:"小狗狗",message:"我是小狗狗", preferredStyle:UIAlertControllerStyle.alert);
        alert.addAction(UIAlertAction(title:"OK",style:UIAlertActionStyle.default,handler:nil));
        
        present(alert,animated: true,completion: nil);
        
        
    }
    @IBAction func show3(){
        let alert = UIAlertController(title:"外星人",message:"我是外星人", preferredStyle:UIAlertControllerStyle.alert);
        alert.addAction(UIAlertAction(title:"OK",style:UIAlertActionStyle.default,handler:nil));
        
        present(alert,animated: true,completion: nil);
        
        
    }
    @IBAction func show4(){
        let alert = UIAlertController(title:"機器人",message:"我是機器人", preferredStyle:UIAlertControllerStyle.alert);
        alert.addAction(UIAlertAction(title:"OK",style:UIAlertActionStyle.default,handler:nil));
        
        present(alert,animated: true,completion: nil);
        
        
    }


    @IBAction func test(_ sender: UIButton) {
        
        let title = sender.currentTitle
       
        print("\(title) was touched")
        
        
    }

}
    

